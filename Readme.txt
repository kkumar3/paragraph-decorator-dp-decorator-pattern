
Assuming you are in the directory containing this README:

## To clean:
ant -buildfile fileProcessorDecorator/src/build.xml clean

-----------------------------------------------------------------------
## To compile: 
ant -buildfile fileProcessorDecorator/src/build.xml all

-----------------------------------------------------------------------
## To run by specifying arguments from command line 

ant -buildfile fileProcessorDecorator/src/build.xml run -Darg0=input.txt -Darg1=output.txt

-----------------------------------------------------------------------

## To create tarball for submission

ant -buildfile fileProcessorDecorator/src/build.xml tarzip

-----------------------------------------------------------------------

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense."

[Date: 08/02/2017]

-----------------------------------------------------------------------

Provide justification for Data Structures used in this assignment in
term of Big O complexity (time and/or space)

I have used string array list to store paragraphs, sentences and words. The linked list provide the feature of expanding the data structures for new elements. 

To store the word frequency, I have used a Hash Map<String, Count>.

To access the data elements of these lists was fairly starightforward with O(n) time complexity. 

-----------------------------------------------------------------------

Provide list of citations (urls, etc.) from where you have taken code
(if any).

