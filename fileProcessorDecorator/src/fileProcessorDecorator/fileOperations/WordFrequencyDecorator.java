package fileProcessorDecorator.fileOperations;

public class WordFrequencyDecorator extends FileProcessorAbstractBase{
	
	FileProcessorAbstractBase wf1;

	public WordFrequencyDecorator(FileProcessorAbstractBase wfIn){
		this.wf1 = wfIn;
	}
	
	public void processFileData(InputDetails idIn){
		
		//store each word in the hash map with appropriate count value
		for(int i=0; i<idIn.words.size();i++){
			if(!idIn.frequency.containsKey(idIn.words.get(i))){
				idIn.frequency.put(idIn.words.get(i),1);
			}
			else{ //repeated occurrence will store the previous count+ 1 as value.
				idIn.frequency.put(idIn.words.get(i), (Integer)idIn.frequency.get(idIn.words.get(i)) + 1 );
			}
		
		}	 
				
		//print the list words in output file
			idIn.writer.println("--- WORDS_FREQUENCY_DECORATOR_START ---");
				
				
			for (String word: idIn.frequency.keySet()){

	            String key = word.toString();
	            String value = idIn.frequency.get(word).toString();  
	            idIn.writer.println(key + " " + value);  

			}
			

			idIn.writer.println("--- WORDS_FREQUENCY_DECORATOR_END ---");
			
			idIn.writer.close();
			
		

	}

}
