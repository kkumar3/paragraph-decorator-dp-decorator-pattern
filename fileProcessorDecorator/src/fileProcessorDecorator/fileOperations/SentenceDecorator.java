package fileProcessorDecorator.fileOperations;

import java.util.Arrays;

public class SentenceDecorator extends FileProcessorAbstractBase{
	
	FileProcessorAbstractBase s1;

	
	public SentenceDecorator(FileProcessorAbstractBase sIn){
		this.s1 =  sIn;
	}
	
	public void processFileData(InputDetails idIn){
		//extract sentences
		String tempSentences[];
	
		for(String inputLine : idIn.paragraphs)
		{
			 tempSentences = inputLine.split("\\.");//("(?<=[.])");
			 
			 idIn.sentences.addAll(Arrays.asList(tempSentences));
		}	
		
		//write all the extracted sentences to output file.
			idIn.writer.println("--- SENTENCE_DECORATOR_START ---");
			
			for(String opString : idIn.sentences){
				idIn.writer.println(opString);
			}
			
			idIn.writer.println("--- SENTENCE_DECORATOR_END ---");
			
			
		//call abstract class method on the decorator set by its constructor
		s1.processFileData(idIn);
	}

}
