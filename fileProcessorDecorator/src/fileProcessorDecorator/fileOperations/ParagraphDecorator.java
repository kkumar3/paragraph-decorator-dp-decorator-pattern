package fileProcessorDecorator.fileOperations;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ParagraphDecorator extends FileProcessorAbstractBase {
	
	FileProcessorAbstractBase p1;

	public ParagraphDecorator(FileProcessorAbstractBase dIn){
		this.p1 = dIn;
	}
	
	public void processFileData(InputDetails idIn){
		
		//get paragraphs
		try {
			File f = new File(idIn.fileName);
			BufferedReader b = new BufferedReader(new FileReader(f));
			
			String inputLine="";
			String tempPara[];
			//read lines one by 1
			
				while ((inputLine = b.readLine())!= null) {
					
					tempPara = inputLine.split("(?<=[.!?])(\\s\\s+|\\n)");
					
					idIn.paragraphs.addAll(Arrays.asList(tempPara));
				}
				
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		
			
			//print the paragraphs read	
				idIn.writer.println("--- PARAGRAPH_DECORATOR_START ---");
				
				for(String opString : idIn.paragraphs){
					idIn.writer.println(opString);
				}
				
				idIn.writer.println("--- PARAGRAPH_DECORATOR_END ---");
				
				
		
			
		//call abstract class method on the decorator set by its constructor
		
		p1.processFileData(idIn);
	}

}
