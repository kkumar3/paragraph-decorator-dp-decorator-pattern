package fileProcessorDecorator.fileOperations;
//abstract base class
public abstract class FileProcessorAbstractBase {
	
	public abstract void processFileData(InputDetails id1);
	
}
