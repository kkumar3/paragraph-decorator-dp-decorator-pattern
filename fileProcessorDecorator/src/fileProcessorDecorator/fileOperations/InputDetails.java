package fileProcessorDecorator.fileOperations;


import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class InputDetails {
	
	String fileName="";
	public String opFile="";
	ArrayList<String> paragraphs = new ArrayList<String>();
	ArrayList<String> sentences = new ArrayList<String>();
	ArrayList<String> words = new ArrayList<String>();
	
	public PrintWriter writer;
	
	
	
	
	Map<String, Integer> frequency = new HashMap<String, Integer>();
	
	public InputDetails(String fileNameIn, String opFileIn){
		fileName = fileNameIn;
		opFile = opFileIn;
		
		try {
			 writer = new PrintWriter(opFile, "UTF-8");
			} catch (FileNotFoundException e) {
				System.err.println("The output file was not found. Please provide an output file");
				e.printStackTrace();
				System.exit(1);
			} catch (UnsupportedEncodingException e) {
				System.err.println("The encoding UTF 8 is not dupported with this output file");
				e.printStackTrace();
				System.exit(1);
			}
	}
	

	
	//APIs to retrieve data members

}
