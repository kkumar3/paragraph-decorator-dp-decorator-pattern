package fileProcessorDecorator.fileOperations;

import java.util.Arrays;

public class WordDecorator extends FileProcessorAbstractBase {
	
	FileProcessorAbstractBase w1;
	
	public WordDecorator(FileProcessorAbstractBase wIn){
		this.w1 = wIn;
	}
	public void processFileData(InputDetails idIn){
		//extract words
		//read each sentence and find words and store them 
		String tempWords[];
		
		for(String inputLine : idIn.sentences){
			tempWords = inputLine.split("\\s+");
			idIn.words.addAll(Arrays.asList(tempWords));
		}
		
		
		//print each word in output file	
		idIn.writer.println("--- WORDS_DECORATOR_START ---");
			
			for(String opString : idIn.words){
				idIn.writer.println(opString);
			}
			
			idIn.writer.println("--- WORDS_DECORATOR_END ---");
			
			
		
		//call abstract class method on the decorator set by its constructor
		w1.processFileData(idIn);
	}

}
