package fileProcessorDecorator.driver;

import fileProcessorDecorator.fileOperations.FileProcessorAbstractBase;
import fileProcessorDecorator.fileOperations.InputDetails;
import fileProcessorDecorator.fileOperations.ParagraphDecorator;
import fileProcessorDecorator.fileOperations.SentenceDecorator;
import fileProcessorDecorator.fileOperations.WordDecorator;
import fileProcessorDecorator.fileOperations.WordFrequencyDecorator;

public class Driver {

	public static void main(String args[]){
		
		//instantiation of InputDetails object with file names.
		InputDetails i1 = new InputDetails(args[0], args[1]);
		

		//calling and instantiation of wrapper classes  
		FileProcessorAbstractBase wordFrequencyDecorator = new WordFrequencyDecorator(null);
		FileProcessorAbstractBase wordDecorator  = new WordDecorator(wordFrequencyDecorator);
		FileProcessorAbstractBase sentenceDecorator = new SentenceDecorator(wordDecorator);
		FileProcessorAbstractBase paragraphDecorator = new ParagraphDecorator(sentenceDecorator);
		
		paragraphDecorator.processFileData(i1);
		

		
	}
}
